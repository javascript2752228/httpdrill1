const http = require("http");
const fs = require("fs");
const uuid = require("uuid");
const port = process.env.PORT || 3000;

function requestHandler(request, response) {
  const { url } = request;
  if (url === "/html") {
    fs.readFile("./Data/index.html", "utf-8", (err, htmlContent) => {
      if (err) {
        response.writeHead(500, { "Content-Type": "text/html" });

        response.end(`Internal Server Error`);
      } else {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.end(htmlContent);
      }
    });
  } else if (url === "/json") {
    fs.readFile("./Data/data.json", "utf-8", (err, jsonData) => {
      if (err) {
        response.writeHead(500, { "Content-Type": "text/html" });

        response.end(`status 500:Internal Server Error`);
      } else {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.end(JSON.stringify(jsonData));
      }
    });
  } else if (url === "/uuid") {
    response.writeHead(200, { "Content-Type": "text/json" });
    let uuidv4 = JSON.stringify({
      uuid: uuid.v4(),
    });
    response.end(uuidv4);
  } else if (request.url.startsWith("/status/")) {
    const statusCode = parseInt(request.url.split("/")[2]);
    if (statusCode >= 100 && statusCode < 200) {
      response.end(`please send a value above 199`);
    } else {
      response.end(`status code is ${statusCode}`);
    }
  } else if (url.startsWith("/delay/")) {
    const delayInSeconds = parseFloat(url.split("/delay/")[1]);

    response.writeHead(200, { "Content-Type": "application/json" });

    setTimeout(() => {
      response.end(JSON.stringify({ status: 200 }));
    }, delayInSeconds * 1000);
  } else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.end(`status 404 not found`);
  }
}

const server = http.createServer(requestHandler);

server.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}/`);
});
